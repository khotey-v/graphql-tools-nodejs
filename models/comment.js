import Sequelize from 'sequelize'
import Conn from '../connector'
import Post from './post'

const Comment = Conn.define('comment', {
  text: { type: Sequelize.STRING, allowNull: false },
})

export default Comment