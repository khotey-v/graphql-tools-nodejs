import Comment from './comment'
import Post from './post'

Comment.belongsTo(Post)
Post.hasMany(Comment)

export {
  Comment,
  Post
}