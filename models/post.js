import Sequelize from 'sequelize'
import Conn from '../connector'
import Comment from './comment'

const Post = Conn.define('post', {
  title: { type: Sequelize.STRING, allowNull: false },
  content: { type: Sequelize.STRING, allowNull: false },
})

export default Post