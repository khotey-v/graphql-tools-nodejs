import Sequelize from 'sequelize'

const Conn = new Sequelize('test', null, null, {
  dialect: 'sqlite',
  storage: './test.sqlite'
})

export default Conn