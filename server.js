import express from 'express'
import graphqlHTTP from 'express-graphql'
import { makeExecutableSchema } from 'graphql-tools'
import schemas from './schemas'
import resolvers from './resolvers'
import connector from './connector'
import seeds from './seeds'

const PORT = process.env.PORT || 3000

const SchemaDefinition = `
  schema {
    query: Query
  }
`;

const schema = makeExecutableSchema({
  typeDefs: [
    SchemaDefinition,
    ...schemas
  ],
  resolvers,
});

const app = express()
app.use('/graphql', graphqlHTTP({ schema, graphiql: true }))

app.listen(PORT, err => {
  if (err) console.log(err)

  console.log(`Server is running on ${PORT} port.`)
})
