import { merge } from 'lodash';
import queryResolver from './query'
import commentResolver from './comment'
import postResolver from './post'

export default merge(
  queryResolver,
  commentResolver,
  postResolver,
)