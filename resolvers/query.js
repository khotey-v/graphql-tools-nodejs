import Post from '../models/post'
import Comment from '../models/comment'

export default {
  Query: {
    posts(_, args, context) {
      return Post.findAll({ where: args })
    },

    comments(_, args) {
      return Comment.findAll({ where: args })
    }
  }
}