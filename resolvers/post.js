export default {
  Post: {
    comments(post, args, context, info) {
      return post.getComments();
    }
  }
}