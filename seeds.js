import _ from 'lodash'
import Faker from 'faker'
import Conn from './connector'
import {
  Post,
  Comment,
} from './models'

Conn
  .sync({ force: true })
  .then(() => {
    _.times(10, () => {
      return Post.create({
        title: Faker.lorem.words(1, 5),
        content: Faker.lorem.paragraphs(1, 3),
      }).then(post => {
        _.times(5, () => {
          post.createComment({
            text: Faker.lorem.words(5, 12)
          })
        })
      })
    })
  })