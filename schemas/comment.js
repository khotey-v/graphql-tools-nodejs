import Post from './post'

const Comment = `
  type Comment {
    id: Int!
    title: String!
    text: String!
    post: Post!
  }
`

export default () => [Comment, Post]