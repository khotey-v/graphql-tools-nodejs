import Query from './query'
import Post from './post'
import Comment from './comment'

export default [
  Query,
  Post,
  Comment,
]