import Post from './post'
import Comment from './comment'

const Query = `
  type Query {
    posts: [Post]
    comments: [Comment]
  }
`

export default () => [Query, Post, Comment]