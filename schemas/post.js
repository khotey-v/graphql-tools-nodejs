import Comment from './comment'

const Post = `
  type Post {
    id: Int!
    title: String!
    content: String!
    comments: [Comment]
  }
`

export default () => [Post, Comment]
